/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors: {
        "primary": {
          50: "#F0F6FF",
          100: "#D1DBE6",
          200: "#A8BED4",
          300: "#6B8CAD",
          400: "#426D97",
          500: "#194D80",
          600: "#164572",
          700: "#133C63",
          800: "#103151",
          900: "#0B2239",
        },
        "monochrome": {
          50: "#F9FAFB",
          100: "#F3F4F6",
          200: "#E5E7EB",
          300: "#D1D5DB",
          400: "#9CA3AF",
          500: "#6B7280",
          600: "#4B5563",
          700: "#374151",
          800: "#1F2937",
          900: "#111827",
        },
        "background": "#F8F8F8",
        "text-secondary": "#B8B8B8",
        "disabled": "#D9D9D9",
        "success": "#28C76F",
        "danger": "#EA5455",
        "warning": "#FF9F43",
      },
      fontFamily: {
        sans: [
          '"Inter"',
          'system-ui',
          '-apple-system',
          'BlinkMacSystemFont',
          '"Segoe UI"',
          'Roboto',
          '"Helvetica Neue"',
          'Arial',
          '"Noto Sans"',
          'sans-serif',
          '"Apple Color Emoji"',
          '"Segoe UI Emoji"',
          '"Segoe UI Symbol"',
          '"Noto Color Emoji"',
        ],
      },
    },
  },
  plugins: [
    require('@tailwindcss/line-clamp'),
  ],
}
