import { AfterViewInit, Component, Input } from '@angular/core';
import * as d3 from 'd3';
import { ChartData, LineData } from 'src/interfaces/chart.interface';

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.scss']
})
export class BarComponent implements AfterViewInit {
  @Input()
  chartName: string = 'bar';

  @Input()
  chartTitle: string = 'Bar Chart';

  private _data: ChartData = {} as ChartData

  @Input() set data(value: ChartData) {
    this._data = value
    this.createSvg();
    this.drawChart(this._data);
  }

  ngAfterViewInit(): void {
    this.createSvg();
    this.drawChart(this._data);
  }

  private svg: any;
  private margin = 50;
  private width = 750 - (this.margin * 2);
  private height = 400 - (this.margin * 2);

  private createSvg(): void {
    this.svg = d3.select(`figure#${this.chartName}`)
    .append("svg")
    .attr("width", this.width + (this.margin * 2))
    .attr("height", this.height + (this.margin * 2))
    .append("g")
    .attr("transform", `translate(${this.margin},${this.margin / 2})`);
  }

  private drawChart(data: ChartData): void {
    const x = d3.scaleBand()
    .range([0, this.width])
    .domain(data.lineData.map((d: LineData) => d.label))
    .padding(0.2)

    this.svg.append("g")
    .attr("transform", `translate(0,${this.height})`)
    .call(d3.axisBottom(x))
    .selectAll("text")
    .attr("transform", `translate(-10,0)rotate(-45)`)
    .style("text-anchor", "end")

    const max = Math.max(...data.lineData.map(item => item.value))

    const y = d3.scaleLinear()
    .domain([0, data.yrange < max ? max : data.yrange])
    .range([this.height, 0])

    this.svg.append("g")
    .call(d3.axisLeft(y))

    var tooltip = d3.select("body")
    .append("div")
    .attr("class", "absolute invisible bg-white rounded-lg py-2 px-3 shadow-sm border")
    .text("a simple tooltip");

    this.svg.selectAll("bars")
    .data(data.lineData)
    .enter()
    .append("rect")
    .attr("x", (d: LineData) => x(d.label))
    .attr("y", (d: LineData) => y(0))
    .attr("width", x.bandwidth())
    .attr("height", (d: LineData) => this.height - y(0))
    .attr("fill", "#d04a35")
    .on("mouseover", (e: any, d: any) => tooltip.style("visibility", "visible").text(d.value))
    .on("mousemove", (e: any, d: any) =>  tooltip.style("top", (e.pageY-10)+"px").style("left",(e.pageX+10)+"px"))
    .on("mouseout", (e: any, d: any) => tooltip.style("visibility", "hidden"));

    this.svg.selectAll("rect")
    .transition()
    .duration(800)
    .attr("y", (d: LineData) => y(d.value))
    .attr("width", x.bandwidth())
    .attr("height", (d: LineData) => this.height - y(d.value))
    .delay((d: any, i: any) => i * 100)
  }

}
