import { AfterViewInit, Component, Input } from '@angular/core';
import * as d3 from 'd3'
import { ChartData, LineData } from 'src/interfaces/chart.interface';

@Component({
  selector: 'app-line',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.scss']
})
export class LineComponent implements AfterViewInit {
  @Input()
  chartName: string = 'bar';

  @Input()
  chartTitle: string = 'Bar Chart';

  private _data: ChartData = {} as ChartData

  @Input() set data(value: ChartData) {
    this._data = value
    this.createSvg();
    this.drawChart(this._data);
  }

  ngAfterViewInit(): void {
    this.createSvg();
    this.drawChart(this._data);
  }

  private svg: any;
  private margin = 50;
  private width = 750 - (this.margin * 2);
  private height = 400 - (this.margin * 2);

  private createSvg(): void {
    this.svg = d3.select(`figure#${this.chartName}`)
    .append("svg")
    .attr("width", this.width + (this.margin * 2))
    .attr("height", this.height + (this.margin * 2))
    .append("g")
    .attr("transform", `translate(${this.margin},${this.margin / 2})`);
  }

  private drawChart(data: ChartData): void {
    const x = d3.scaleBand()
    .range([0, this.width])
    .domain(data.lineData.map((d: LineData) => d.label))
    .padding(0.2)

    this.svg.append("g")
    .attr("transform", `translate(0,${this.height})`)
    .call(d3.axisBottom(x))
    .selectAll("text")
    .attr("transform", `translate(-10,0)rotate(-45)`)
    .style("text-anchor", "end")

    const max = Math.max(...data.lineData.map(item => item.value))

    const y = d3.scaleLinear()
    .domain([0, data.yrange < max ? max : data.yrange])
    .range([this.height, 0])

    this.svg.append("g")
    .call(d3.axisLeft(y))

    var tooltip = d3.select("body")
    .append("div")
    .attr("class", "absolute invisible bg-white rounded-lg py-2 px-3 shadow-sm border")
    .text("a simple tooltip");

    var focus = this.svg.append("g")
    .attr("class", "focus bg-white")
    .style("display", "none");

    focus.append("circle")
    .attr("r", 5);

    this.svg.append("rect")
    .attr("class", "bg-white")
    .attr("fill", "transparent")
    .attr("width", this.width)
    .attr("height", this.height)
    .on("mouseover", (e: any, d: any) => tooltip.style("visibility", "visible"))
    .on("mousemove", (e: any, d: any) => {
      const chart = document.getElementById(this.chartName)
      const chartBound = chart?.getBoundingClientRect()
      const eachBand = x.step();
      const index = Math.round((((e.x - (chartBound?.x ?? 0) - this.margin) - 28) / eachBand));
      const val = x.domain()[index];
      const i = d3.bisectCenter(x.domain(), val)
      const dat = data.lineData[i]
      return tooltip.style("top", ((y(dat.value) ?? 0) + (chartBound?.y ?? 0) + (this.margin / 2))+"px").style("left", ((x(dat.label) ?? 0) + (chartBound?.x ?? 0) + this.margin + 28)+"px").text(dat.value)
    })
    .on("mouseout", (e: any, d: any) => tooltip.style("visibility", "hidden"));

    const path = this.svg
    .append("path")
    .attr("id", this.chartName+"_line")
    .datum(data.lineData)
    .attr("fill", "none")
    .attr("stroke", "steelblue")
    .attr("stroke-width", 1.5)
    .attr("d", d3.line()
      .x((d: any): number => (x(d.label) ?? 0) + 28)
      .y((d: any): number => y(d.value) ?? 0)
    )

    const pathLength = path.node()?.getTotalLength()

    const transitionPath = d3
    .transition()
    .ease(d3.easeSin)
    .duration(2500)

    path
    .attr("stroke-dashoffset", pathLength)
    .attr("stroke-dasharray", pathLength)
    .transition(transitionPath)
    .attr("stroke-dashoffset", 0)
  }
}
