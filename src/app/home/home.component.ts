import { Component, OnInit } from '@angular/core';
import { ChartData, LineData } from 'src/interfaces/chart.interface';
import * as d3 from 'd3'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  anakStuntingPerTahun: ChartData = {
    yrange: 200000,
    lineData: [

    ],
  };

  anakStuntingPerKabKota2021: ChartData = {
    yrange: 200000,
    lineData: [

    ],
  };

  pemberianASIEkslusifPerKabKota2021: ChartData = {
    yrange: 200000,
    lineData: [

    ],
  };

  anakStuntingPerTahunLoading: boolean = true
  anakStuntingPerKabKota2021Loading: boolean = true
  pemberianASIEkslusifPerKabKota2021Loading: boolean = true

  ngOnInit(): void {
    d3.json("https://opendata.jabarprov.go.id/api-backend/bigdata/dinkes/od_17147_jumlah_balita_stunting_berdasarkan_kabupatenkota?limit=216").then((data: any) => {
      const years = data.metadata_filter.find((item: any) => item.key === 'tahun').value
      const lineData: LineData[] = years.map((year: any) => {
        return {
          label: year,
          value: data.data.filter((item: any) => item.tahun === year).reduce((n: any, { jumlah_balita_stunting }: {jumlah_balita_stunting: number}) => n + jumlah_balita_stunting, 0)
        }
      })
      this.anakStuntingPerTahunLoading = false
      this.anakStuntingPerTahun = {
        yrange: 200000,
        lineData: lineData,
      }
    })

    d3.json('https://opendata.jabarprov.go.id/api-backend/bigdata/dinkes/od_17147_jumlah_balita_stunting_berdasarkan_kabupatenkota?limit=216&where={"tahun":2021}').then((data: any) => {
      const kabKotas = data.metadata_filter.find((item: any) => item.key === 'nama_kabupaten_kota').value
      const lineData: LineData[] = kabKotas.map((kabKota: any) => {
        return {
          label: kabKota,
          value: data.data.find((item: any) => item.nama_kabupaten_kota === kabKota).jumlah_balita_stunting
        }
      })
      this.anakStuntingPerKabKota2021Loading = false
      this.anakStuntingPerKabKota2021 = {
        yrange: 0,
        lineData: lineData,
      }
    })

    d3.json('https://opendata.jabarprov.go.id/api-backend/bigdata/dinkes/od_17369_persentase_pemberian_air_susu_ibu_asi_eksklusif_pada_b?limit=216&where={"tahun":2021}').then((data: any) => {
      const kabKotas = data.metadata_filter.find((item: any) => item.key === 'nama_kabupaten_kota').value
      const lineData: LineData[] = kabKotas.map((kabKota: any) => {
        return {
          label: kabKota,
          value: data.data.find((item: any) => item.nama_kabupaten_kota === kabKota).persentase_pemberian_asi
        }
      })
      this.pemberianASIEkslusifPerKabKota2021Loading = false
      this.pemberianASIEkslusifPerKabKota2021 = {
        yrange: 100,
        lineData: lineData,
      }
    })
  }
}
