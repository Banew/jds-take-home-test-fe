FROM node:16.14-alpine as build
WORKDIR /app

RUN npm install -g @angular/cli

COPY ./package.json .
RUN npm install
COPY . .
RUN ng build

FROM nginx:1.23.3-alpine as runtime

COPY --from=build /app/dist/jds-take-home-test-fe /usr/share/nginx/html
